const electron = require('electron');
const { app, BrowserWindow } = electron;

const path = require('path');
const url = require('url');

let win;

function createWindow() {
    win = new BrowserWindow({
        webPreferences: {
            nodeIntegration: true
        },
        toolbar: false,
        width: 600,
        height: 180,
        //height: 600,
        minWidth: 300,
        minHeight: 100,
        movable: true,
    });

    win.setMenu(null);

    // FOR DEBUG
    //win.webContents.openDevTools();

    win.loadFile(path.join(__dirname, 'index.html'));

    win.on('closed', () => {
        win = null;
    });
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
    if (process.platform != 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    if (win == null) {
        createWindow();
    }
});