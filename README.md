<div align="center">
    <img src="assets/icons/png/512x512.png" alt="drawing" height="200">
</div>

# YGOPro HQ Cards Downloader

[![Build Status](https://img.shields.io/gitlab/pipeline/G_Roux/ygopro-hq-cards)](https://gitlab.com/G_Roux/ygopro-hq-cards/commits/master)
[![Twitter](https://img.shields.io/twitter/url?label=Follow%20me&style=social&url=https%3A%2F%2Ftwitter.com%2FManiakTesteur)](https://twitter.com/ManiakTesteur)

A Downloader for the cards from YGOProDeck.

# Features

* Download all the card pics render available on [YgoProDeck](https://db.ygoprodeck.com/)
* Check already existing pics for fastest updates
* High resolution cards (old resolution: **177x254** => new resolution: **421x614**)
* Check existing files weight to know if it needs to be re-downloaded

# How to use

* Choose the folder where you want to save all the images (you should use a clean folder for your first use)
* Click on the download button then wait for the download to be completed (~10 minutes for 10 035 cards)
* Move all images in your ` ygopro/pics` folder if you didn't choose it for download
* Enjoy :)

(You might need to check from time to time if new cards are available at better resolution, for this remove the old pics and re-download it)

# How to run

``` bash
npm install
npm start
```

# How to install

## Windows

Simply download the latest release build and execute the .exe file.

## MacOS and Linux

Build the application using electron packager :

``` bash
npm install
npm run package-mac
```

or

``` bash
npm run package-linux
```

**Warning: You need to have [NodeJs](https://nodejs.org/en/) installed !**

# TODO

* Translate the application in French with configuration files
* Taskbar icon when the windows is reduced (to track download info)

# Credits

* [YGOPRODECK](https://ygoprodeck.com/) - _API_
* [Guillaume Roux](https://gitlab.com/G_Roux) - _Initial work_

