var app = require('electron').remote;
var dialog = app.dialog;
var request = require('request');
var Filequeue = require('filequeue');
var fs = require('fs');

var fq = new Filequeue(50);

const httpreq = new XMLHttpRequest();

var dirToSave;
var dlIndex = 0;
var cardLoad;

var dlProgress = document.getElementById('dlProgress');
var objLength;

var cardToDl = [];

function downloadFile(fileUrl, targetPath) {
    var req = request({
        method: 'GET',
        uri: fileUrl
    });

    var out = fq.createWriteStream(targetPath);
    req.pipe(out);

    req.on('end', () => {
        dlProgress.innerHTML = "Downloaded: " + dlIndex + "/" + objLength;
    });
}

class CardObj {
    constructor(id, dlUrl) {
        this.id = id;
        this.dlUrl = dlUrl;
    }

    download() {
        downloadFile(this.dlUrl, dirToSave + "/" + this.id + ".jpg");
    }

    get doesExists() {
        var path = dirToSave + "/" + this.id + ".jpg";
        var returnVal = fs.existsSync(path);
        if (returnVal) {
            var stats = fs.statSync(path);
            var fileSizeInBytes = stats.size;
            if (fileSizeInBytes < 30000) return false;
        }
        return returnVal;
    }
}

function loadXcards(nb) {
    if (dlIndex < objLength) {
        for (var i = 0; i < nb && dlIndex < objLength; i++) {
            var cardExists = cardToDl[dlIndex].doesExists;
            while (cardExists && dlIndex < objLength - 1) {
                dlProgress.innerHTML = "Downloaded: " + dlIndex + "/" + objLength;
                dlIndex++;
                cardExists = cardToDl[dlIndex].doesExists;
            }
            cardToDl[dlIndex].download();
            dlIndex++;
        }
    } else {
        clearInterval(cardLoad);
        dlProgress.innerHTML = "Downloaded: " + dlIndex + "/" + objLength;
        alert("Your download is completed.");
    }
}

function getCards() {
    httpreq.open('GET', 'https://db.ygoprodeck.com/api/v7/cardinfo.php', false);
    httpreq.send(null);

    if (httpreq.status === 200) {
        obj = JSON.parse(httpreq.responseText);
        obj.data.forEach(card => {
            card.card_images.forEach(img => {
                cardToDl.push(new CardObj(img.id, img.image_url));
            });
        });
        objLength = cardToDl.length;
        dlProgress.innerHTML = "Downloaded: " + dlIndex + "/" + objLength;
    } else {
        alert("Error: Couldn't retrieve image from YgoProDeck.");
        console.log("Response Status: %d (%s)", httpreq.status, httpreq.statusText);
    }
}

document.getElementById('createButton').onclick = () => {
    dialog.showOpenDialog({
        properties: ['openDirectory']
    }, (directoryName) => {
        if (directoryName === undefined) return;
        dirToSave = directoryName;
        document.getElementById('pathToSave').value = directoryName;
    });
};

document.getElementById('downloadButton').onclick = () => {
    if (dirToSave === undefined) {
        alert("Please choose a directory to save the images.");
        return;
    }
    cardLoad = setInterval(() => {
        loadXcards(17);
    }, 1200);
};

getCards();
